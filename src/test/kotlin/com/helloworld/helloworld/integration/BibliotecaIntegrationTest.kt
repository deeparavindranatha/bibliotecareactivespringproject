package com.helloworld.helloworld.integration

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import com.helloworld.helloworld.service.BibliotecaService
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Test
import org.springframework.test.web.servlet.MockMvc
import org.springframework.beans.factory.annotation.Autowired
import reactor.core.publisher.Mono


@WebMvcTest

class BibliotecaIntegrationTest {
    @Autowired
    private val mockMvc: MockMvc? = null

    private val service = mock<BibliotecaService>()

    @Test
    fun `should get welcome message`() {
        whenever(service.getWelcomeMessage()).thenReturn(Mono.just("Welcome to Biblioteca"))

//        mockMvc.perform(
//                get("/biblioteca/home"))
//                .andExpect(status().isOk())
//                .andExpect(content().string("Welcome To Biblioteca"));

    }

}