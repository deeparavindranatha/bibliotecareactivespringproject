package com.helloworld.helloworld.service

import com.helloworld.helloworld.data.AddBookRequest
import com.helloworld.helloworld.model.Book
import com.helloworld.helloworld.repository.BibliotecaRepository
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.whenever
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.times
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.util.UUID

@Tag("unit")
internal class BibliotecaServiceTest {
    private val repository = mock<BibliotecaRepository>()

    private val service = BibliotecaService(repository)

    private val harryPotter = Book(UUID.fromString("65cf3c7c-f449-4cd4-85e1-bc61dd2db64f"),
            "Harry Potter",
            "J.K.Rowling", 1998, "978-1-56619-909-4")
    private val enchantedWoods = Book(UUID.fromString("65cf3c7c-f449-4cd4-85e1-bc61dd2db64e"),
            "Enchanted Woods",
            "Enid Blyton", 1998, "978-1-56619-909-5")

    @Test
    fun `should return welcome message`() {

        val expected = "Welcome to Biblioteca"
        StepVerifier.create(service.getWelcomeMessage())
                .expectNext(expected)
                .verifyComplete()
    }

    @Disabled
    @Test
    fun `should call the save method of repository to create a new asset`() {
        val bookReq = AddBookRequest("Harry Potter", "J.K.Rowling", 1998, "978-1-56619-909-4")

        val captor = argumentCaptor<Book> {
            whenever(repository.save(capture())).thenReturn(Mono.just(Book(firstValue.id, bookReq.name, bookReq.author, bookReq.publishedYear, bookReq.isbn)))
        }

        StepVerifier.create(service.create(bookReq))
                .expectNext(captor.firstValue)
                .verifyComplete()

        verify(repository, times(1)).save(harryPotter)
    }

    @Test
    fun `should get all books`() {
        whenever(repository.findAll()).thenReturn(
                Flux.just(enchantedWoods, harryPotter))

        StepVerifier.create(service.getBooks())
                .expectNext(enchantedWoods, harryPotter)
                .verifyComplete()

        verify(repository, times(1)).findAll()
    }

    @Test
    fun `should get book with given id`() {
        whenever(repository.findById(UUID.fromString("65cf3c7c-f449-4cd4-85e1-bc61dd2db64e"))).thenReturn(
                Mono.just(enchantedWoods))

        StepVerifier.create(service.getBookById(enchantedWoods.id))
                .expectNext(enchantedWoods)
                .verifyComplete()

        verify(repository, times(1)).findById(enchantedWoods.id)
    }

    @Test
    fun `should return empty Book with invalid book id`() {
        val invalidId = UUID.randomUUID()
        whenever(repository.findById(invalidId)).thenReturn(
                Mono.empty<Book>())

        StepVerifier.create(service.getBookById(invalidId))
                .verifyComplete()

        verify(repository, times(1)).findById(invalidId)
    }
}