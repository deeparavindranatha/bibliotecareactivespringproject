package com.helloworld.helloworld.controller

import com.helloworld.helloworld.data.AddBookRequest
import com.helloworld.helloworld.model.Book
import com.helloworld.helloworld.repository.BibliotecaRepository
import com.helloworld.helloworld.service.BibliotecaService
import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.util.UUID

@Tag("unit")
internal class BibliotecaControllerTest {
    val repository = mock<BibliotecaRepository>()

    private val service = mock<BibliotecaService>()

    private val controller = BibliotecaController(service)

    private val harryPotter = Book(UUID.fromString("65cf3c7c-f449-4cd4-85e1-bc61dd2db64f"),
            "Harry Potter",
            "J.K.Rowling", 1998, "978-1-56619-909-4")
    private val enchantedWoods = Book(UUID.fromString("65cf3c7c-f449-4cd4-85e1-bc61dd2db64e"),
            "Enchanted Woods",
            "Enid Blyton", 1998, "978-1-56619-909-5")

    @Test
    fun `should return hello biblioteca`() {

        whenever(service.getWelcomeMessage()).thenReturn(
                Mono.just("Welcome to Biblioteca"))

        StepVerifier.create(controller.hello())
                .expectNext(ResponseEntity.ok("Welcome to Biblioteca"))
                .verifyComplete()

        verify(service, times(1)).getWelcomeMessage()
    }

    @Test
    fun `should save book`() {
        val bookReq = AddBookRequest("Harry Potter", "J.K.Rowling", 1998, "978-1-56619-909-4")
        val id = UUID.fromString("65cf3c7c-f449-4cd4-85e1-bc61dd2db64f")
        whenever(service.create(bookReq)).thenReturn(
                Mono.just(harryPotter))

        StepVerifier.create(controller.addBook(bookReq))
                .expectNext(ResponseEntity.status(HttpStatus.CREATED).body(id))
                .verifyComplete()

        verify(service, times(1)).create(bookReq)
    }

    @Test
    fun `should get all books`() {
        whenever(service.getBooks()).thenReturn(
                Flux.just(enchantedWoods, harryPotter))

        val books = listOf(enchantedWoods, harryPotter)
        StepVerifier.create(controller.getBooks())
                .expectNext(ResponseEntity.status(HttpStatus.OK).body(books))
                .verifyComplete()

        verify(service, times(1)).getBooks()
    }

    @Test
    fun `should get book with given id`() {
        whenever(service.getBookById(enchantedWoods.id)).thenReturn(
                Mono.just(enchantedWoods))

        StepVerifier.create(controller.getBookById(enchantedWoods.id))
                .expectNext(ResponseEntity.ok(enchantedWoods))
                .verifyComplete()

        verify(service, times(1)).getBookById(enchantedWoods.id)
    }

    @Test
    fun `should throw BookNotFoundException with invalid book id`() {
        val invalidId = UUID.randomUUID()
        whenever(service.getBookById(invalidId)).thenReturn(
                Mono.empty())

        StepVerifier.create(controller.getBookById(invalidId))
                .expectNext(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null))
                .verifyComplete()

        verify(service, times(1)).getBookById(invalidId)
    }


}