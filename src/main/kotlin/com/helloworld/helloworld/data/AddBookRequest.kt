package com.helloworld.helloworld.data

data class AddBookRequest(val name:String, val author:String, val publishedYear:Int, val isbn: String)