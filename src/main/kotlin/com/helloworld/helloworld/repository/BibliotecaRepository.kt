package com.helloworld.helloworld.repository

import com.helloworld.helloworld.model.Book
import org.springframework.stereotype.Repository
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository
import java.util.UUID


@Repository
interface BibliotecaRepository : ReactiveCassandraRepository<Book, UUID>