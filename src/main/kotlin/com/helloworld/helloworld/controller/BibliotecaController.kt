package com.helloworld.helloworld.controller

import com.helloworld.helloworld.data.AddBookRequest
import com.helloworld.helloworld.data.BookDetailResponse
import com.helloworld.helloworld.model.Author
import com.helloworld.helloworld.model.Book
import com.helloworld.helloworld.service.BibliotecaService
import kotlinx.coroutines.reactive.collect
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PathVariable
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import sun.security.util.Password
import java.lang.Exception
import java.util.UUID

@Controller
@RequestMapping("/biblioteca")
class BibliotecaController(private val service: BibliotecaService) {

    @GetMapping("/hello")
    fun hello(): Mono<ResponseEntity<String>> {
        return service.getWelcomeMessage().map {
            ResponseEntity(it, HttpStatus.OK)
        }
    }

    @PostMapping("/books")
    fun addBook(@RequestBody book: AddBookRequest): Mono<ResponseEntity<UUID>> {
        return service.create(book).map { savedBook ->
            ResponseEntity(savedBook!!.id, HttpStatus.CREATED)
        }.defaultIfEmpty(ResponseEntity(HttpStatus.CONFLICT))
    }

    @GetMapping("/books")
    fun getBooks(): Mono<ResponseEntity<List<Book>>> {
        return service.getBooks().collectList().map {
            ResponseEntity(it, HttpStatus.OK)
        }
    }


    @GetMapping("/books/{id}")
    fun getBookById(@PathVariable id: UUID): Mono<ResponseEntity<Book>> {
        return service.getBookById(id).map { book ->
            ResponseEntity(book, HttpStatus.OK)
        }.defaultIfEmpty(ResponseEntity(HttpStatus.NOT_FOUND))
    }


    @GetMapping("/bookauthors")
    fun getBookAuthors(): Mono<ResponseEntity<List<UUID>>> {
        return service.getAuthorsByBookId(UUID.fromString("65cf3c7c-f449-4cd4-85e1-bc61dd2db64e")).collectList().map {
            ResponseEntity(it, HttpStatus.OK)
        }
    }

    @GetMapping("/authors")
    fun getAuthors(): Mono<ResponseEntity<List<Author>>> {
        return service.getAuthor().collectList().map {
            ResponseEntity(it, HttpStatus.OK)
        }
    }

    @GetMapping("/booksInDetail")
    fun getBooksInDetail(): Mono<ResponseEntity<List<BookDetailResponse>>> {
        return service.getBookDetails().collectList().map {
            ResponseEntity(it, HttpStatus.OK)
        }
    }
}