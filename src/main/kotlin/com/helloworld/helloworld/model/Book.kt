package com.helloworld.helloworld.model

import org.jetbrains.annotations.NotNull
import org.springframework.data.cassandra.core.mapping.Column
import org.springframework.data.cassandra.core.mapping.PrimaryKey
import java.util.UUID

data class Book(
        @NotNull @PrimaryKey val id: UUID = UUID(0, 0),
        val name: String,
        val author: String,
        @Column("year_of_publish") val yearOfPublish : Int,
        val isbn : String)