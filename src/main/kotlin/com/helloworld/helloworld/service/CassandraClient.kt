package com.helloworld.helloworld.service

import com.datastax.driver.core.querybuilder.QueryBuilder
import com.helloworld.helloworld.model.Author
import org.springframework.data.cassandra.ReactiveSession
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Component
class CassandraClient (private val reactiveSession: ReactiveSession) {
    fun getAuthor(): Flux<Author> {
        return reactiveSession.execute(
                QueryBuilder.select().all().from("Author")
        ).flatMapMany {
            reactiveResultSet ->
            if(!reactiveResultSet.wasApplied()) throw Exception()
            else reactiveResultSet.rows().map { row ->
                Author(row)
            }
        }
    }
     fun getAuthorsByBookId(id: UUID) :  Flux<UUID>{
         return reactiveSession.execute(QueryBuilder.select().all().from("BookAuthor")
                 .where(QueryBuilder.eq("id", id))
         ).flatMapMany {
             reactiveResultSet ->
             if(!reactiveResultSet.wasApplied()) throw Exception()
             else reactiveResultSet.rows().map { row ->
                 row.getUUID("author_id")
             }
         }
     }

    fun getAuthorInfoById(author_id : UUID) : Mono<Author>{
        return reactiveSession.execute(
                QueryBuilder.select().all().from("Author")
                        .where(QueryBuilder.eq("author_id", author_id))
        ).flatMap{
            reactiveResultSet ->
            if(!reactiveResultSet.wasApplied()) throw Exception()
            else reactiveResultSet.rows().next().map { row ->
                Author(row)
            }
        }
    }
}